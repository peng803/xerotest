I made the changes using Visual Studio 2017

The existing code has a few problems:

1. The code is not well structured. The code for accessing the database is tightly coupled with logic code. 

2. The code for accessing the database is an "old school style". Including sql query directly in a string is not good. It's very easy to make mistakes. 

I have created a folder called "DataRepository", and anything inside this folder is used to get the data, so that the data is ready for the logic layer (controller). It basically replaces the function of the "Models" folder. I didn't make any changes to the "Models" folder. Ideally, if I'm going to create a new project like this, I will put everything inside the "DataRepository" folder into the "Models" folder, and there is no need to create the "DataRepository" folder.  

I have also split the product and product option to two seperate controllers, as they are seperate data tables.

The "GenericRepository" class contains the general CRUD operations. 
The "UnitOfWork" class will ensure multiple repositories use the same data context class. 