﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using refactor_this.Models;
using refactor_me.DataRepository;


namespace refactor_me.Controllers
{
    [RoutePrefix("products")]
    public class ProductOptionsController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();  // this contains all the data collections from the data repository layer

        [Route("{productId}/options")]
        [HttpGet]
        public IEnumerable<refactor_me.DataRepository.ProductOption> GetOptions(Guid productId)
        {
            return unitOfWork.ProductOptionRepository.Get(filter: po=>po.ProductId==productId);
        }

        [Route("{productId}/options/{id}")]
        [HttpGet]
        public refactor_me.DataRepository.ProductOption GetOption(Guid productId, Guid id)
        {
            var searchResult = unitOfWork.ProductOptionRepository.GetByID(id);

            if (searchResult == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return searchResult;
        }

        [Route("{productId}/options")]
        [HttpPost]
        public void CreateOption(Guid productId, refactor_me.DataRepository.ProductOption option)
        {
            option.ProductId = productId;

            if (unitOfWork.ProductOptionRepository.GetByID(option.Id) != null)
            {
                unitOfWork.ProductOptionRepository.Delete(option.Id);
                unitOfWork.ProductOptionRepository.Insert(option);
            }
            else
            {
                unitOfWork.ProductOptionRepository.Insert(option);
            }

            unitOfWork.Save();
        }

        [Route("{productId}/options/{id}")]
        [HttpPut]
        public void UpdateOption(Guid id, refactor_me.DataRepository.ProductOption option)
        {
            if (unitOfWork.ProductOptionRepository.GetByID(option.Id) != null)
            {
                unitOfWork.ProductOptionRepository.Delete(option.Id);
                unitOfWork.ProductOptionRepository.Insert(option);
            }
            else
            {
                unitOfWork.ProductOptionRepository.Insert(option);
            }

            unitOfWork.Save();
        }

        [Route("{productId}/options/{id}")]
        [HttpDelete]
        public void DeleteOption(Guid id)
        {
            unitOfWork.ProductOptionRepository.Delete(id);
            unitOfWork.Save();
        }
    }
}
