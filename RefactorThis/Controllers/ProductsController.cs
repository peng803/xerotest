﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using refactor_this.Models;
using refactor_me.DataRepository;
using System.Linq;

namespace refactor_this.Controllers
{
    [RoutePrefix("products")]
    public class ProductsController : ApiController
    {
        private UnitOfWork unitOfWork = new UnitOfWork();  // this contains all the data collections from the data repository layer

        [Route]
        [HttpGet]
        public IEnumerable<refactor_me.DataRepository.Product> GetAll()
        {
            return unitOfWork.ProductRepository.Get();
        }

        [Route("SearchByName/{name}")]
        [HttpGet]
        public IEnumerable<refactor_me.DataRepository.Product> SearchByName(string name)  
        {
            return unitOfWork.ProductRepository.Get(filter: p=>p.Name.Contains(name)).ToList(); 
        }

        [Route("SearchByID/{id}")]
        [HttpGet]
        public refactor_me.DataRepository.Product GetProduct(Guid id)
        {
            var searchResult = unitOfWork.ProductRepository.GetByID(id);

            if (searchResult == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            
            return searchResult;
        }

        [Route]
        [HttpPost]
        public void Create(refactor_me.DataRepository.Product product)
        {
            if (unitOfWork.ProductRepository.GetByID(product.Id) != null)
            {
                unitOfWork.ProductRepository.Delete(product.Id);
                unitOfWork.ProductRepository.Insert(product);
            }
            else
            {
                unitOfWork.ProductRepository.Insert(product);
            }

            unitOfWork.Save();
        }

        [Route("{id}")]
        [HttpPut]
        public void Update(Guid id,refactor_me.DataRepository.Product product)
        {
            if (unitOfWork.ProductRepository.GetByID(id)!=null)
            {
                unitOfWork.ProductRepository.Delete(id);
                unitOfWork.ProductRepository.Insert(product);
            }
            else
            {
                unitOfWork.ProductRepository.Insert(product);
            }

            unitOfWork.Save();
        }

        [Route("{id}")]
        [HttpDelete]
        public void Delete(Guid id)
        {
            unitOfWork.ProductRepository.Delete(id);
            unitOfWork.Save();
        }
    }
}
