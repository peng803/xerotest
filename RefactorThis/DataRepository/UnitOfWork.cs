﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using refactor_me.DataRepository;

namespace refactor_me.DataRepository
{
    public class UnitOfWork : IDisposable
    {

        //this class can make sure we use a consistent data context to update the data.

        private DatabaseDbContext context = new DatabaseDbContext();

        private GenericRepository<Product> productRepository;
        private GenericRepository<ProductOption> productOptionRepository;

        public GenericRepository<Product> ProductRepository
        {
            get
            {

                if (this.productRepository == null)
                {
                    this.productRepository = new GenericRepository<Product>(context);
                }
                return productRepository;
            }
        }

        public GenericRepository<ProductOption> ProductOptionRepository
        {
            get
            {

                if (this.productOptionRepository == null)
                {
                    this.productOptionRepository = new GenericRepository<ProductOption>(context);
                }
                return productOptionRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}